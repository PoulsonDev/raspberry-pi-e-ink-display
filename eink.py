import logging
from draw import create_image

from PIL import Image,ImageDraw,ImageFont
import epd2in7b
import time
logging.basicConfig(level=logging.DEBUG)
black_image, red_image = create_image()

epd = epd2in7b.EPD()
epd.init()
epd.Clear()
time.sleep(1)

# Drawing on the image
logging.info("Drawing")
# HBlackimage = Image.new('1', (epd.height, epd.width), 255)  # 298*126
# HRedimage = Image.new('1', (epd.height, epd.width), 255)  # 298*126
# drawblack = ImageDraw.Draw(HBlackimage)
# drawred = ImageDraw.Draw(HRedimage)
#
# font24 = ImageFont.truetype('SourceSansPro-Bold.ttf', 24)
#
# drawblack.text((10, 0), 'hello world', font = font24, fill = 0)
# drawblack.text((10, 20), '2.9inch e-Paper', font = font24, fill = 0)
# drawblack.line((20, 50, 70, 100), fill = 0)
# drawblack.line((70, 50, 20, 100), fill = 0)
# drawred.line((165, 50, 165, 100), fill=0)
# drawred.line((140, 75, 190, 75), fill=0)
# drawred.arc((140, 50, 190, 100), 0, 360, fill=0)
# drawred.rectangle((80, 50, 130, 100), fill=0)
# drawred.chord((200, 50, 250, 100), 0, 360, fill=0)
epd.display(epd.getbuffer(black_image), epd.getbuffer(red_image))

# print("Clearing...")
# epd.Clear(0xFF)

# epd.display(epd.getbuffer(black_image), epd.getbuffer(red_image))

epd.sleep()
