# Raspberry Pi Home Assistant E-ink Display

Displays data from Home Assistant API, uses the Waveshare E-ink displays.

![Example Display](example-display.png)

## Requirements

- Raspberry Pi (should work on even Zero model, requires GPIO pins though)
- Waveshare E-ink Display (currently hardcoded to use [the 2.7 inch 3-color, model](https://www.waveshare.com/wiki/2.7inch_e-Paper_HAT_(B)), can be modified to use other models)
- Python 3
- Home Assistant (running on your network, you need the weather sensor and Calendar component)

## Installation

- Make sure the display is connected
- Clone this repo
- `pip3 install -r requirements.txt`
- Copy `.env.example` to `.env`
- Modify `.env` to use your API credentials etc
- Run `eink.py` to update the screen

If you don't have the display connected, you can test to code by running `test.py` file.
